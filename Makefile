all: serialdump

clean:
	rm -f serialdump
	rm -f *.o

serialdump: serialdump.o
	gcc -o serialdump serialdump.o

serialdump.o: serialdump.c
	gcc -c serialdump.c
