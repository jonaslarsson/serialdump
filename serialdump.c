/* serialdump
 *
 * Usage: serialdump /dev/ttyS1 115200
 *
 * Setting custom baud rates
 * http://stackoverflow.com/questions/19440268/how-to-set-a-non-standard-baudrate-on-a-serial-port-device-on-linux
 *
 *
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <linux/serial.h>
#include <asm/ioctls.h>
#include <time.h>

const char *l_programName = 0;

void printUsage(const char *programName)
{
    printf("Usage: %s [options] serialport baudrate\n", programName);
}

void printHelp(const char *programName)
{
    printUsage(programName);
    printf("\n");
    printf("This is a simple command line tool for dumping data from serial ports.\n");
    printf("\n");
    printf("Options:\n");
    printf("  -h, --help   show this help page\n");
    printf("  --hex        output data as hex\n");
    printf("\n");
    printf("Not all serial drivers will support non standard baudrates.\n");
    printf("\n");
    printf("The standard baudrates are: 50, 75, 110, 134, 150, 200, 300, 600,\n");
    printf("                            1200, 1800, 2400, 4800, 9600, 19200,\n");
    printf("                            38400, 57600, 115200, 230400, 460800,\n");
    printf("                            500000, 576000, 921600, 1000000, 1152000,\n");
    printf("                            1500000, 2000000, 2500000, 3000000,\n");
    printf("                            3500000, 4000000\n");
    printf("\n");
    printf("Example usage:\n");
    printf("  serialdump /dev/ttyS0 115200\n");
}

static int rateToConstant(int baudrate)
{
    switch(baudrate)
    {
    case 50:
        return B50;
    case 75:
        return B75;
    case 110:
        return B110;
    case 134:
        return B134;
    case 150:
        return B150;
    case 200:
        return B200;
    case 300:
        return B300;
    case 600:
        return B600;
    case 1200:
        return B1200;
    case 1800:
        return B1800;
    case 2400:
        return B2400;
    case 4800:
        return B4800;
    case 9600:
        return B9600;
    case 19200:
        return B19200;
    case 38400:
        return B38400;
    case 57600:
        return B57600;
    case 115200:
        return B115200;
    case 230400:
        return B230400;
    case 460800:
        return B460800;
    case 500000:
        return B500000;
    case 576000:
        return B576000;
    case 921600:
        return B921600;
    case 1000000:
        return B1000000;
    case 1152000:
        return B1152000;
    case 1500000:
        return B1500000;
    case 2000000:
        return B2000000;
    case 2500000:
        return B2500000;
    case 3000000:
        return B3000000;
    case 3500000:
        return B3500000;
    case 4000000:
        return B4000000;
    default:
        return 0;
    }
}

// Open serial port in raw mode, with custom baudrate if necessary
int serialOpen(const char *device, int rate)
{
    struct termios options;
    struct serial_struct serinfo;
    int fd;
    int speed = 0;

    fd = open(device, O_RDWR | O_NOCTTY);
    if (fd < 0)
    {
	err(1, "failed to open serial device");
    }


    speed = rateToConstant(rate);

    if (speed == 0)
    {
	// Non standard baudrate, use divisor
        serinfo.reserved_char[0] = 0;
        if (ioctl(fd, TIOCGSERIAL, &serinfo) < 0)
	{
	    err(1, "failed to set custom baudrate");
	}
        serinfo.flags &= ~ASYNC_SPD_MASK;
        serinfo.flags |= ASYNC_SPD_CUST;
        serinfo.custom_divisor = (serinfo.baud_base + (rate / 2)) / rate;
        if (serinfo.custom_divisor < 1)
            serinfo.custom_divisor = 1;
        if (ioctl(fd, TIOCSSERIAL, &serinfo) < 0)
	{
	    err(1, "failed to set custom baudrate");
	}
        if (ioctl(fd, TIOCGSERIAL, &serinfo) < 0)
	{
	    err(1, "failed to set custom baudrate");
	}
        if (serinfo.custom_divisor * rate != serinfo.baud_base)
	{
            warnx("using divisor %d, actual baudrate is %d / %d = %f",
		  serinfo.custom_divisor,
                  serinfo.baud_base, serinfo.custom_divisor,
                  (float)serinfo.baud_base / serinfo.custom_divisor);
        }
    }

    fcntl(fd, F_SETFL, 0);
    tcgetattr(fd, &options);
    cfsetispeed(&options, speed ?: B38400);
    cfsetospeed(&options, speed ?: B38400);
    cfmakeraw(&options);
    options.c_cflag |= (CLOCAL | CREAD);
    options.c_cflag &= ~CRTSCTS;
    if (tcsetattr(fd, TCSANOW, &options) != 0)
    {
	err(1, "failed to apply serial options");
    }

    return fd;
}

unsigned long timeMsec()
{
    static time_t l_tv_sec = 0;
    static suseconds_t l_tv_usec = 0;
    unsigned long msec;
    struct timeval toc;

    gettimeofday(&toc, 0);
    if (l_tv_sec == 0)
    {
	/* First time called, set the time to 0 */
	l_tv_sec = toc.tv_sec;
	l_tv_usec = toc.tv_usec;
    }

    msec = (toc.tv_sec - l_tv_sec) * 1000;
    msec += (toc.tv_usec - l_tv_usec) / 1000;
    return msec;
}

void printTimestamp()
{
    unsigned long msec = timeMsec();

    unsigned long seconds = msec / 1000;
    msec %= 1000;

    unsigned long minutes = seconds / 60;
    seconds %= 60;

    unsigned long hours = minutes / 60;
    minutes %= 60;
    
    printf("%02u:%02u:%02u.%03u:\n", hours, minutes, seconds, msec);
}

void hexdump(const char *buf, ssize_t size)
{
    int i;
    printTimestamp();
    for (i = 0; i < size; i++)
    {
	printf("%02x ", (unsigned char)buf[i]);
	if (i % 16 == 15) printf("\n");
    }
    if (i % 16 != 0) printf("\n");
    printf("\n");
}

void serialDump(int fd)
{
    char buf[1024];
    while (1)
    {
	ssize_t size = read(fd, buf, sizeof(buf));
	if (size == -1)
	{
	    err(1, "read failed");	    
	}
	if (size > 0)
	{
	    hexdump(buf, size);
	}
    }
}

// Returns value, or 0 on error
int stringToInt(const char *str)
{
    int value = atoi(str);
    char buf[32];
    if (value <= 0)
    {
	return 0;
    }
    sprintf(buf, "%d", value);
    if (strcmp(buf, str) == 0)
    {
	return value;
    }
    return 0;
}

int main(int argc, char **argv)
{
    int c;
    static int hexFlag = 0;
    l_programName = argv[0];

    static struct option longOptions[] =
    {
        {"hex", no_argument, &hexFlag, 1},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}
    };

    while (1)
    {
        int optionIndex = 0;
        c = getopt_long(argc, argv, "h", longOptions, &optionIndex);

        if (c == -1)
        {
            // No more options to parse
            break;
        }

        switch (c)
        {
        case 0:
            break;
        case 'h':
            printHelp(argv[0]);
            return 0;
        case '?':
            /* getopt_long already printed an error message. */
            break;
        default:
            abort ();
        }
    }

    if (hexFlag)
        puts ("hex flag is set");


    if (argc - optind != 2)
    {
	printUsage(argv[0]);
	return -1;
    }

    const char *device = argv[optind];
    int baudrate = stringToInt(argv[optind + 1]);

    if (baudrate <= 0)
    {
	fprintf(stderr, "%s: invalid baudrate\n", argv[0]);
	return -1;
    }

    int fd = serialOpen(device, baudrate);

    if (fd > 0)
    {
	serialDump(fd);
    }

    return 0;
}
